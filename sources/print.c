//
// Created by david on 1/6/18.
//

#include "../headers/functions.h"

void print_array(Object *);

void print(Object *o, bool newline) {
    if(!print_symbol)
        print_symbol = true;
    if(newline)
        printf(" => ");
    if(getTag(o) == T_VARIABLE)
        error("Print received what it should not receive");
    switch(getTag(o)) {
        case T_INTEGER: {
            printf("%ld", o->integer.value);
            break;
        }
        case T_STRING: {
            printf("\"%s\"", o->string.value);
            break;
        }
        case T_ARRAY: {
            print_array(o);
            break;
        }
        case T_NULL: {
            printf("NULL");
            break;
        }
        case T_FUNCTION: {
            printf("%s", o->function.name);
            break;
        }
        case T_BOOL: {
            if(o->boolean.state)
                printf("True");
            else
                printf("False");
            break;
        }
        default: {
            break;
        }
    }
    if(newline)
        printf("\n");
}

void print_array(Object *array) {
    if(array->array.max_index == -1) {
        printf("[]");
    }
    else {
        printf("[ ");
        int i = 0;
        for (; i < array->array.max_index; i++) {
            print(array->array.array->array[i], false);
            printf(", ");
        }
        print(array->array.array->array[i], false);
        printf(" ]");
    }
}
