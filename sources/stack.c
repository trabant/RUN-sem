//
// Created by david on 1/3/18.
//

#include "../headers/stack.h"
#include "../headers/helper_collections.h"

Stack* new_stack() {
    Stack *stack = (Stack*)malloc(sizeof(Stack));
    stack->stack = new_array();
    insert_stack_to_helpers_heap(stack);
    return stack;
}

Stack* new_global_stack() {
    Stack *stack = (Stack*)malloc(sizeof(Stack));
    stack->stack = new_array();
    return stack;
}

void push(Stack *stack, Object *o) {
    Object *item = o;
    set_item(stack->stack, stack->stack->size, item);
    stack->stack->size++;
    if(full_array(stack->stack))
        grow_array(stack->stack);
}

void pop(Stack *stack) {
    stack->stack->array[stack->stack->size-1] = NULL;
    stack->stack->size--;
}

Object *top(Stack *stack) {
    return stack->stack->array[stack->stack->size-1];
}

bool empty_stack(Stack *stack) {
    return empty_array(stack->stack);
}