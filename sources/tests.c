//
// Created by david on 1/15/18.
//
#include "../headers/object.h"
#include "../headers/functions.h"

void test(bool cond, const char *message) {
    if(!cond) {
        fprintf(stderr, "Test failed: %s\n", message);
        abort();
    }
}

void run_tests() {
    Object *o = new_string("Ahoj");
    test(getTag(o) == T_STRING, "String has T_STRING tag");
    test(equal_strings(o->string.value, "Ahoj"), "String has correct value");
    test(o->string.length == 4, "String has correct length");

    Object *o1 = new_integer(10);
    test(getTag(o1) == T_INTEGER, "Integer has T_INTEGER tag");
    test(o1->integer.value == 10, "Integer has correct value");

    Object *o10 = new_integer(-10);
    test(o10->integer.value == -10, "Integer can be negative");

    Object *o2 = new_operator('+');
    test(getTag(o2) == T_OPERATOR, "Operator has T_OPERATOR tag");
    test(o2->operator.operator == '+', "Operator has correct char");

    Object *o80 = new_array_object();
    test(getTag(o80) == T_ARRAY, "Array has T_ARRAY tag");
    test(o80->array.max_index == -1, "New array has default max index -1");

    Object *o3 = new_integer(20);
    Object *o4 = new_node(o2, o1, o3);
    test(getTag(o4) == T_NODE, "Node has T_NODE tag");
    test(getTag(o4->node.operator) == T_OPERATOR, "Node has operator");
    test(o4->node.operator->operator.operator == '+', "Node has right operator");
    test(getTag(o4->node.left) == T_INTEGER && o4->node.left->integer.value == 10, "Node has correct object on left");
    test(getTag(o4->node.right) == T_INTEGER && o4->node.right->integer.value == 20, "Node has correct object on right");

    o = new_string_stream("4 + 5;");
    Stack* expr_stack = my_read(o);
    test(getTag(top(expr_stack)) == T_NODE && expr_stack->stack->size == 1, "Read returns 1 node");
    test(*o->string_stream.input == '\0', "Read reached end of string stream input");

    Object *to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 9, "REPL sums integers correctly");

    o = new_string_stream("4 - 5;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == -1, "REPL subtracts integers correctly");

    o = new_string_stream("4 * 5;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 20, "REPL multiplicates integers correctly");

    o = new_string_stream("4 / 2;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 2, "REPL divides integers correctly");

    o = new_string_stream("4 + 2 * 5;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 14, "Priority works");

    o = new_string_stream("4 * (2 + 5);");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 28, "Brackets works");

    o = new_string_stream("a = 5;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 5, "REPL assigns integers correctly");

    o = new_string_stream("a;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 5, "Assigned variable is saved correctly");

    o = new_string_stream("a = 7;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 7, "Assigned variable can be changed");

    o = new_string_stream("b = a;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 7, "Variable can be assigned to variable");

    o = new_string_stream("b = 4;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 4, "New variable can be changed");

    o = new_string_stream("a;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 4, "Shallow copy works with integers");

    o = new_string_stream("a + b;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 8, "Variables can be summed");

    o = new_string_stream("a - b;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 0, "Variables can be subtracted");

    o = new_string_stream("a * b;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 16, "Variables can be multiplicated");

    o = new_string_stream("a / b;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 1, "Variables can be divided");

    o = new_string_stream("3 * (a * b) + 2 * ( 8 - 11) + 0;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 42, "More complicated math with variables works too");

    o = new_string_stream("def aa(a, b) { a = b + 5; a * 2; }");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_FUNCTION && equal_strings(to_print->function.name, "aa"), "New function can be defined");

    o = new_string_stream("aa(1, 16);");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 42, "Defined function can be called");

    o = new_string_stream("aa(20, 10);");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 30, "Defined function can be called multiple times correctly");

    o = new_string_stream("aa(20, 10) * 10;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 300, "Basic math operations (*) work with functions");

    o = new_string_stream("aa(20, 10) + 10;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 40, "Basic math operations (+) work with functions");

    o = new_string_stream("aa(20, 10) - 10;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 20, "Basic math operations (-) work with functions");

    o = new_string_stream("aa(20, 10) - aa(20,10);");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 0, "Basic math operations work between functions");

    o = new_string_stream("def bb(a, b) { c = a + b; c; }");
    expr_stack = my_read(o);
    eval(expr_stack);
    o = new_string_stream("bb(2, 4);");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 6, "Function variable can be assigned");

    o = new_string_stream("def cc(a, b) { bb(a,a) + bb(b, b); }");
    expr_stack = my_read(o);
    eval(expr_stack);
    o = new_string_stream("cc(2, 4);");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 16, "Functions see other functions");

    o = new_string_stream("a = 10;");
    expr_stack = my_read(o);
    eval(expr_stack);
    o = new_string_stream("while(a) { a = a - 1; }");
    expr_stack = my_read(o);
    eval(expr_stack);
    o = new_string_stream("a;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 0, "While works");

    o = new_string_stream("if(1) { 1; else 2; }");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 1, "Condition with true works correctly");

    o = new_string_stream("if(0) { 1; else 2; }");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 2, "Condition with false works correctly");

    o = new_string_stream("if(1) { 1; }");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 1, "Condition with true without else works correctly");

    o = new_string_stream("if(a) { 1; else 0; }");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 0, "Condition with variable works correctly");

    o = new_string_stream("if(0) { 1; }");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_NULL, "Condition with false and without else return null");

    o = new_string_stream("a = 10;");
    expr_stack = my_read(o);
    eval(expr_stack);
    o = new_string_stream("if(a) { 1; else 0; }");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 1, "Condition with true without else works correctly");

    o = new_string_stream("1 < 2;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_BOOL && to_print->boolean.state == true, "Less then works correctly");

    o = new_string_stream("3 < 2;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_BOOL && to_print->boolean.state == false, "Less then works correctly");

    o = new_string_stream("3 < 3;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_BOOL && to_print->boolean.state == false, "Less then works correctly");

    o = new_string_stream("1 > 2;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_BOOL && to_print->boolean.state == false, "Higher then works correctly");

    o = new_string_stream("3 > 2;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_BOOL && to_print->boolean.state == true, "Higher then works correctly");

    o = new_string_stream("3 > 3;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_BOOL && to_print->boolean.state == false, "Less then works correctly");

    o = new_string_stream("3 == 3;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_BOOL && to_print->boolean.state == true, "Equals works correctly");

    o = new_string_stream("2 == 3;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_BOOL && to_print->boolean.state == false, "Equals works correctly");

    o = new_string_stream("3 == 2;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_BOOL && to_print->boolean.state == false, "Equals works correctly");

    o = new_string_stream("2 + 3 == 5 * 2 - 5;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_BOOL && to_print->boolean.state == true, "Complex comparisons works correctly");

    o = new_string_stream("(5 * 1) > (2 + 3 == 5 * 2 - 5) + 4;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_BOOL && to_print->boolean.state == false, "Complex comparisons works correctly");

    o = new_string_stream("\"aa\" > \"a\" == \"a\" < \"aa\";");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_BOOL && to_print->boolean.state == true, "String comparisons works correctly");

    o = new_string_stream("a = 10;");
    expr_stack = my_read(o);
    eval(expr_stack);
    o = new_string_stream("a > 5;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_BOOL && to_print->boolean.state == true, "Comparisons with variables works");

    o = new_string_stream("\"a\" + \"a\";");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_STRING && equal_strings(to_print->string.value, "aa"), "Add string to string works");

    o = new_string_stream("\"a\" + 5;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_STRING && equal_strings(to_print->string.value, "a5"), "Add int to string works");

    o = new_string_stream("5 + \"a\";");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_STRING && equal_strings(to_print->string.value, "5a"), "Add int to string works");

    o = new_string_stream("6 * \"a\";");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_STRING && equal_strings(to_print->string.value, "aaaaaa"), "Multiplying string from left works");

    o = new_string_stream("\"a\" * 3;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_STRING && equal_strings(to_print->string.value, "aaa"), "Multiplying string from right works");

    o = new_string_stream("load_file \"test\"");
    my_read(o);
    o = new_string_stream("sum(1,2);");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 3, "Definitions from other files can be loaded");

    o = new_string_stream("r = [];");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_ARRAY, "New array can be initialized");

    o = new_string_stream("r[0];");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_NULL, "Default value for array field is T_NULL");

    o = new_string_stream("r[480];");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_NULL, "Array dynamically grows when needed");

    o = new_string_stream("r[0] = 10;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 10, "New value can be assigned to array field");

    o = new_string_stream("r[1] = 5;");
    expr_stack = my_read(o);
    eval(expr_stack);
    o = new_string_stream("r[1] + r[0];");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 15, "Array fields can take part in math operations (+)");

    o = new_string_stream("r[1] - r[0];");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == -5, "Array fields can take part in math operations (-)");

    o = new_string_stream("r[1] * r[0];");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 50, "Array fields can take part in math operations (*)");

    o = new_string_stream("i = 0;");
    expr_stack = my_read(o);
    eval(expr_stack);
    o = new_string_stream("r[i];");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 10, "Array can be accessed from value in variable");

    o = new_string_stream("r[i] = [];");
    expr_stack = my_read(o);
    eval(expr_stack);
    o = new_string_stream("r[i][0] = 1;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 1, "Multi-dimensional arrays can be assigned");

    o = new_string_stream("r[i][0] + 5;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 6, "Multi-dimensional arrays work correctly");

    o = new_string_stream("def f(){10;}");
    expr_stack = my_read(o);
    eval(expr_stack);
    o = new_string_stream("r[f()];");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_NULL, "Array index can be function call");

    o = new_string_stream("def g(a){a;}");
    expr_stack = my_read(o);
    eval(expr_stack);
    o = new_string_stream("g(f());");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 10, "Function can have function call input as arg");

    o = new_string_stream("if(f() == 10) { 1; }");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 1, "Condition can have function call input as arg");

    o = new_string_stream("// This is just a comment\n 4;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_INTEGER && to_print->integer.value == 4, "Comments are ignored");

    o = new_string_stream("def aa() {}");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_FUNCTION && equal_strings(to_print->function.name, "aa"), "Functions can be redefined defined");

    o = new_string_stream("aa();");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_NULL, "Functions without parametres and body can be defined and called");

    o = new_string_stream("a = 1;");
    expr_stack = my_read(o);
    eval(expr_stack);
    o = new_string_stream("b = a;");
    expr_stack = my_read(o);
    eval(expr_stack);
    o = new_string_stream("a = \"Ahoj\";");
    expr_stack = my_read(o);
    eval(expr_stack);
    o = new_string_stream("b;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_STRING && equal_strings("Ahoj", to_print->string.value), "Shallow copy works with strings");

    o = new_string_stream("a = [];");
    expr_stack = my_read(o);
    eval(expr_stack);
    o = new_string_stream("b;");
    expr_stack = my_read(o);
    to_print = eval(expr_stack);
    test(getTag(to_print) == T_ARRAY && to_print->array.max_index == -1, "Shallow copy works with arrays");

    printf("All tests passed!\n");
    free_all();
    prepare_to_run();
}