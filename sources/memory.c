//
// Created by david on 1/23/18.
//

#include "../headers/array.h"
#include "../headers/defaults.h"
#include "../headers/environment.h"
#include "../headers/functions.h"
#include "../headers/function_data.h"
#include "../headers/helper_collections.h"
#include "../headers/buffer.h"

void mark_array(Array *);

Helpers_collection* new_helpers_heap() {
    Helpers_collection *col = (Helpers_collection *)malloc(sizeof(Helpers_collection));
    col->field = (helper_field *)malloc(HELPERS_HEAP_SIZE * sizeof(helper_field));
    col->stack_pos = col->buff_pos = 0;
    return col;
}

int max(int a, int b) {
    return a > b ? a : b;
}

int min(int a, int b) {
    return a < b ? a : b;
}

void free_stack(Stack *stack) {
    if(stack->stack) //Array might have been assigned to condition or function
        free_array(stack->stack);
    free(stack);
}

void free_helpers() {
    int stack_count = helpers_heap_table->stack_pos;
    int buff_count = helpers_heap_table->buff_pos;
    for(int i = 0; i < min(stack_count, buff_count); i++) {
        free(helpers_heap_table->field[i].buffer->buffer);
        free(helpers_heap_table->field[i].buffer);
        free_stack(helpers_heap_table->field[i].stack);
    }
    if(stack_count > buff_count) {
        for(int i = buff_count; i < stack_count; i++) {
            free_stack(helpers_heap_table->field[i].stack);
        }
    }
    else {
        for(int i = stack_count; i < buff_count; i++) {
            free(helpers_heap_table->field[i].buffer->buffer);
            free(helpers_heap_table->field[i].buffer);
        }
    }
    helpers_heap_table->stack_pos = helpers_heap_table->buff_pos = 0;
}

void free_environment(Environment *env) {
    if(env->variables)
        free_hash_table(env->variables);
    if(env->functions)
        free_hash_table(env->functions);
    free(env);
}

bool free_object(Object *o) {
    switch(getTag(o)) {
        case T_STRING: {
            free(o->string.value);
            break;
        }
        case T_STRING_STREAM: {
            free(o->string_stream.origin);
            break;
        }
        case T_FILE_STREAM: {
            fclose(o->file_stream.input);
            break;
        }
        case T_VARIABLE: {
            free(o->variable.name);
            if(o->variable.index)
                free_array(o->variable.index);
            break;
        }
        case T_NODE: {
            break;
        }
        case T_FUNCTION: {
            free(o->function.name);
            if(o->function.function_data->args)
                free_array(o->function.function_data->args);
            if(o->function.function_data->code)
                free_array(o->function.function_data->code);
            if(o->function.owns_env && o->function.function_data->func_env && o->function.function_data->func_env != global_env)
                free_environment(o->function.function_data->func_env);
            free(o->function.function_data);
            break;
        }
        case T_CONDITION: {
            if(o->condition.else_code)
                free_array(o->condition.else_code);
            if(o->condition.if_code)
                free_array(o->condition.if_code);
            break;
        }
        case T_ARRAY: {
            if(o->array.array)
                free_array(o->array.array);
            break;
        }
        case T_NULL:
        default: {

        }
    }
    free(o);
    return true;
}

Array* new_heap() {
    Array *array = (Array*)malloc(sizeof(Array));
    array->array = (Object**)malloc(sizeof(Object*) * HEAP_SIZE);
    for(int i = 0; i < HEAP_SIZE; i++)
        array->array[i] = NULL;
    array->capacity = HEAP_SIZE;
    array->size = 0;
    return array;
}


void store_on_heap(Object *o) {
    for(int i = 0; i < HEAP_SIZE; i++) {
        if(object_heap_table->array[i] == NULL) {
            object_heap_table->array[i] = o;
            object_heap_table->size++;
            break;
        }
    }
}

void mark_function(Object *);

void mark_variable(Object *var) {
    if(!var->tag.marked)
        var->tag.marked = true;
    switch(getTag(var)) {
        case T_NODE :{
            mark_variable(var->node.operator);
            if(var->node.left)
                mark_variable(var->node.left);
            if(var->node.right)
                mark_variable(var->node.right);
            break;
        }
        case T_CONDITION: {
            if(var->condition.if_code)
                mark_array(var->condition.if_code);
            if(var->condition.else_code)
                mark_array(var->condition.else_code);
            mark_variable(var->condition.condition);
            break;
        }
        case T_FUNCTION: {
            mark_function(var);
            break;
        }
        default: {

        }
    }
}

void mark_variables(HashTable *table) {
    for(int i = 0; i < table->capacity; i++) {
        if(table->table[i].value != NULL) {
            mark_variable(table->table[i].value);
        }
    }
}

void mark_array(Array *array) {
    if(!array)
        return;
    for(int i = 0; i < array->capacity; i++) {
        if(array->array[i] != NULL) {
            if(array->array[i]->tag.marked)
                continue;
            mark_variable(array->array[i]);
        }
    }
}

void mark_stack(Stack *stack) {
    if(!stack)
        return;
    for(int i = 0; i < stack->stack->size; i++) {
        if(stack->stack->array[i]->tag.marked)
            continue;
        mark_variable(stack->stack->array[i]);
    }
}

void mark_function(Object *func) {
    func->tag.marked = true;
    if(func->function.function_data->func_env)
        mark_variables(func->function.function_data->func_env->variables);
    mark_array(func->function.function_data->code);
    mark_array(func->function.function_data->args);
}

void mark_functions(HashTable *table) {
    for(int i = 0; i < table->capacity; i++) {
        if(table->table[i].value != NULL) {
            if(table->table[i].value->tag.marked)
                continue;
            mark_function(table->table[i].value);
        }
    }
}

void mark() {
    mark_variable(currently_evaluated);
    mark_stack(current_input_streams);
    mark_variables(global_env->variables);
    mark_functions(global_env->functions);
}

void sweep() {
    for(int i = 0; i < HEAP_SIZE; i++) {
        if(object_heap_table->array[i] != NULL) {
            if(!object_heap_table->array[i]->tag.marked) {
                free_object(object_heap_table->array[i]);
                object_heap_table->array[i] = NULL;
                object_heap_table->size--;
            }
            else {
                object_heap_table->array[i]->tag.marked = false;
            }
        }
    }
}

void start_garbage_collection() {
    printf("Starting garbage collector\n");
    mark();
    sweep();
    if(max(helpers_heap_table->buff_pos, helpers_heap_table->stack_pos) > HELPERS_HEAP_LIMIT)
        free_helpers();
}

void free_all() {
    printf("Free all\n");
    for(int i = 0; i < HEAP_SIZE; i++) {
        if(object_heap_table->array[i] != NULL) {
            if(free_object(object_heap_table->array[i])) {
                object_heap_table->array[i] = NULL;
                object_heap_table->size--;
            }
        }
    }
    free_helpers();
    free(helpers_heap_table->field);
    free(helpers_heap_table);
    free(null);
    free_environment(global_env);
    free_array(object_heap_table);
    free_stack(current_input_streams);
    free(my_true);
    free(my_false);
}