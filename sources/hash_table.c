//
// Created by david on 1/7/18.
//

#include "../headers/hash_table.h"
#include "../headers/functions.h"

long next_prime(long num) {
    int i = 0;
    while(primes[i++] > num);
    return primes[i];
}

long hash(char *str, long mod) {
    //djb2
    long h = 5381;
    int c = *str++;
    while(c != '\0') {
        h = ((h << 5) + h) + c;
        c = *str++;
    }
    long res = h % mod;
    return res < 0 ? -res : res;
}

void fill_with_null(HashTable *table) {
    for(int i = 0; i < table->capacity; i++) {
        table->table[i].value = NULL;
        table->table[i].key = NULL;
        table->table[i].toomstomb = false;
    }
}

HashTable *init_hash_table() {
    HashTable *table = (HashTable*)malloc(sizeof(HashTable));
    table->capacity = INIT_HASH_TABLE_SIZE;
    table->size = 0;
    table->table = (HashField*)malloc(table->capacity * sizeof(HashField));
    fill_with_null(table);
    return table;
}

HashTable *new_hash_table(long capacity) {
    HashTable *table = (HashTable*)malloc(sizeof(HashTable));
    table->capacity = capacity;
    table->size = 0;
    table->table = (HashField*)malloc(table->capacity * sizeof(HashField));
    fill_with_null(table);
    return table;
}

void grow_hash_table(HashTable *table) {
    HashTable *new_table = new_hash_table(next_prime(table->capacity * 2));
    for(int i = 0; i < table->capacity; i++) {
        insert_into_hash_table(new_table, table->table[i].key, table->table[i].value);
    }
    table->table = new_table->table;
    table->capacity = new_table->capacity;
}

long find_position(HashTable *table, char *key) {
    long pos = hash(key, table->capacity);
    if(table->table[pos].value == NULL)
        return pos;
    while(table->table[pos].toomstomb || table->table[pos].value != NULL) {
        if(table->table[pos].toomstomb) {
            pos++;
            continue;
        }
        if(equal_strings(table->table[pos].key, key))
            break;
        pos++;
    }
    return pos;
}

void create_new_in_hashtable(HashField *table, long pos, Object * value) {
    switch(getTag(value)) {
        case T_INTEGER: {
            table[pos].value = new_integer(value->integer.value);
            break;
        }
        case T_STRING: {
            table[pos].value = new_string(value->string.value);
            break;
        }
        case T_ARRAY: {
            table[pos].value = new_array_object();
            if(value->array.array == NULL) {
                table[pos].value->array.array = new_array();
            }
            else
                table[pos].value = value;
            break;
        }
        default: {
            error("Unimplemented assignment to variable or function");
        }
    }
}

Object* insert_into_hash_table(HashTable *table, char *key, Object *value) {
    long pos = find_position(table, key);
    bool new = false;
    if(getTag(value) != T_FUNCTION && table->table[pos].value == NULL) {
        table->table[pos].value = NULL;
        table->size++;
        new = true;
    }
    switch(getTag(value)) {
        case T_VARIABLE: {
            table->table[pos].value = find_item(table, value);
            break;
        }
        case T_FUNCTION: {
            table->table[pos].value = value;
            break;
        }
        default: {
            if(new)
                create_new_in_hashtable(table->table, pos, value);
            else
                shallow_copy(table->table[pos].value, value);
        }
    }
    if (!table->table[pos].key) {
        table->table[pos].key = (char *) malloc((strlen(key) + 1) * sizeof(char));
        strcpy(table->table[pos].key, key);
    }
    if(table->table[pos].toomstomb)
        table->table[pos].toomstomb = false;

    if (3 * table->capacity < 4 * table->size)
        grow_hash_table(table);
    return table->table[pos].value;
}

Object *find_item(HashTable *table, Object *item) {
    char *name;
    if(getTag(item) == T_VARIABLE)
        name = item->variable.name;
    else
        name = item->function.name;
    long pos = find_position(table, name);
    return table->table[pos].value == NULL ? null : table->table[pos].value;
}

void delete_from_hash_table(HashTable *table, char *key) {
    long pos = find_position(table, key);
    if(table->table[pos].value == NULL)
        return;
    free(table->table[pos].key);
    table->table[pos].key = NULL;
    table->table[pos].toomstomb = true;
    table->table[pos].value = NULL;
    table->size--;
}

void free_hash_table(HashTable *table) {
    for(int i = 0; i < table->capacity; i++) {
        if(table->table[i].value != NULL) {
            free(table->table[i].key);
        }
    }
    free(table->table);
    free(table);
}
