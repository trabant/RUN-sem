//
// Created by david on 1/3/18.
//

#include <stdbool.h>
#include <ctype.h>
#include "../headers/object.h"
#include "../headers/functions.h"
#include "../headers/function_data.h"
#include "../headers/buffer.h"

void stream_read(Object *input_stream, Stack *expr_stack, Stack *operator_stack);

static inline void check_and_mark_eof(int c, Object *input_stream) {
    if (c == EOF)
        input_stream->file_stream.eof = true;
}

int read_next(Object *input_stream) {
    if(getTag(input_stream) == T_STRING_STREAM)
        return *input_stream->string_stream.input++;
    return getc(input_stream->file_stream.input);
}

void unread(Object *input_stream, int c) {
    if(getTag(input_stream) == T_FILE_STREAM)
        ungetc(c, input_stream->file_stream.input);
    else
        input_stream->string_stream.input--;
}

int stream_peek(Object *input_stream) {
    int c = read_next(input_stream);
    unread(input_stream, c);
    return c;
}

bool is_white_space(Object *input_stream) {
    int c = stream_peek(input_stream);
    check_and_mark_eof(c, input_stream);
    return isspace(c) && c != EOF;
}

bool is_variable(int c) {
    return isalpha(c) || c == '_';
}

bool is_digit(Object *input_stream) {
    int c = stream_peek(input_stream);
    check_and_mark_eof(c, input_stream);
    return isdigit(c) && c != EOF;
}

bool is_digit_or_negative(Object *input_stream, int c) {
    return isdigit(c) || (c == '-' && is_digit(input_stream));
}

bool is_operator(int c) {
    switch (c) {
        case '+':
        case '-':
        case '*':
        case '(':
        case ')':
        case '=':
        case '<':
        case '>':
        case '/':
            return true;
        default:
            return false;
    }
}

bool end_of_string(Object *input_stream) {
    int c = stream_peek(input_stream);
    check_and_mark_eof(c, input_stream);
    return c == '\'' || c == '\"' || c == EOF;
}

void skip_whitespace(Object *input_stream) {
    while(is_white_space(input_stream)) {
        read_next(input_stream);
    }
}

bool stream_finished(Object *input_stream) {
    if(getTag(input_stream) == T_FILE_STREAM)
        return input_stream->file_stream.eof;
    return input_stream->string_stream.input[0] == '\0';
}

void read_comment(Object *input_stream) {
    int c = read_next(input_stream);
    while(!end_of_stream(input_stream) && c != '\n')
        c = read_next(input_stream);
}

bool end_of_stream(Object *input_stream) {
    if(stream_finished(input_stream))
        return true;
    int c = stream_peek(input_stream);
    if(getTag(input_stream) == T_FILE_STREAM) {
        if(c == EOF) {
            input_stream->file_stream.eof = true;
            return true;
        }
        return false;
    }
    return c == '\0';
}

bool read_finished(Object *input_stream, Stack *expr_stack, Stack *operator_stack) {
    return stream_finished(input_stream) || ((expr_stack->stack->size == 1 || expr_stack->stack->size == 0)  && empty_stack(operator_stack));
}

void read_string(Object *input_stream, Stack *expr_stack) {
    int c;
    Buffer *b = new_buffer();
    while(!end_of_string(input_stream)) {
        c = read_next(input_stream);
        add_to_buffer(b, c);
    }
    close_buffer(b);
    if(!input_stream->file_stream.eof)
        read_next(input_stream);
    push(expr_stack, new_string(b->buffer));
}

void read_number(Object *input_stream, Stack *expr_stack, int c) {
    long num;
    bool negative = false;
    if(c == '-') {
        num = 0;
        negative = true;
    }
    else
        num = c - '0';

    while (is_digit(input_stream)) {
        num *= 10;
        num += read_next(input_stream) - '0';
    }
    if(negative)
        num = -num;
    push(expr_stack, new_integer(num));
}

int deal_with_longer_operators(Object *input_stream, int c) {
    switch(c) {
        case '=': {
            if(stream_peek(input_stream) == '=') {
                read_next(input_stream);
                return '~'; //Equivalent to ==
            }
            break;
        }
        default: {
            break;
        }
    }
    return c;
}

void read_operator(Object *input_stream, Stack *expr_stack, Stack *operator_stack, int c) {
    c = deal_with_longer_operators(input_stream, c);
    Object *op = new_operator((char)c);
    while(!empty_stack(operator_stack) && top(operator_stack)->operator.operator != '(' && top(operator_stack)->operator.precedence >= op->operator.precedence) {
        Object *operator = top(operator_stack);
        pop(operator_stack);
        Object *ex2 =top(expr_stack);
        pop(expr_stack);
        Object *ex1 = top(expr_stack);
        pop(expr_stack);
        push(expr_stack, new_node(operator, ex1, ex2));
    }
    push(operator_stack, op);
}

Object *store_args(Object *input_stream) {
    Buffer *b = new_buffer();
    bool string = false;
    int open_bracket = 1;
    while(open_bracket) {
        int c = read_next(input_stream);
        switch(c) {
            case '(': {
                open_bracket++;
                add_to_buffer(b, c);
                break;
            }
            case ')': {
                open_bracket--;
                if(open_bracket)
                    add_to_buffer(b, c);
                break;
            }
            case ',': {
                if(string || open_bracket > 1)
                    add_to_buffer(b, ',');
                else
                    add_to_buffer(b, ';');
                break;
            }
            case '"': {
                string = !string;
            }
            default: {
                add_to_buffer(b, c);
            }
        }
    }
    add_to_buffer(b, ';');
    close_buffer(b);
    return new_string_stream(b->buffer);
}

void read_args(Object *input_stream, Stack *arg_stack, Stack *func_op) {
    while(!end_of_stream(input_stream)) {
        stream_read(input_stream, arg_stack, func_op);
    }
}

void read_body(Object *input_stream, Stack *func_expr, Stack *func_op) {
    skip_whitespace(input_stream);
    if(read_next(input_stream) != '{')
        error("No function body given");
    int lines = 0;
    bool in_function = true;
    while(!end_of_stream(input_stream) && in_function) {
        skip_whitespace(input_stream);
        int c = stream_peek(input_stream);
        switch(c) {
            case '}': {
                in_function = false;
                read_next(input_stream);
                break;
            }
            default: {
                stream_read(input_stream, func_expr, func_op);
                lines++;
            }
        }
    }

    if(func_expr->stack->size != lines || in_function) // Finished by }
        error("Bad input (function body did not end properly)");
}


bool next_is_else(Object *input_stream) {
    int i = 0;
    Buffer *b = new_buffer();
    while(i < 4) {
        if(end_of_stream(input_stream))
            break;
        int c = read_next(input_stream);
        add_to_buffer(b, c);
        i++;
    }
    close_buffer(b);
    if (equal_strings(b->buffer, "else"))
        return true;
    while(i) {
        unread(input_stream, b->buffer[i++]);
    }
    return false;
}

void read_condition_body(Object *input_stream, Object *cond) {
    skip_whitespace(input_stream);
    if(read_next(input_stream) != '{')
        error("No condition body given");
    Stack *cond_expr = new_stack();
    Stack *cond_op = new_stack();
    bool in_condition = true;
    bool in_else = false;
    while(!end_of_stream(input_stream) && in_condition) {
        skip_whitespace(input_stream);
        int c = stream_peek(input_stream);
        switch(c) {
            case '}': {
                in_condition = false;
                read_next(input_stream);
                break;
            }
            case 'e': {
                if(! in_else && next_is_else(input_stream)) {
                    cond->condition.if_code = cond_expr->stack;
                    cond_expr->stack = NULL;
                    cond_expr = new_stack();
                    in_else = true;
                    break;
                } //if not true then default
            }
            default: {
                stream_read(input_stream, cond_expr, cond_op);
            }
        }
    }
    if(in_else)
        cond->condition.else_code = cond_expr->stack;
    else
        cond->condition.if_code = cond_expr->stack;
    cond_expr->stack = NULL;
    if(in_condition)
        error("Bad input (condition did not end properly)");

}

Object *read_condition(Object *input_stream, Stack *arg_stack) {
    if (arg_stack->stack->size != 1)
        error("Condition must have exactly one argument");
    Object *cond = new_condition();
    cond->condition.condition = top(arg_stack);
    read_condition_body(input_stream, cond);
    return cond;
}

Object *read_func(Object *input_stream, char *name, bool *defining) {
    Object *func;
    Stack *arg_stack = new_stack();
    Stack *func_op = new_stack();

    Object *args = store_args(input_stream);
    read_args(args, arg_stack, func_op);
    if(equal_strings(name, "if")) {
        func = read_condition(input_stream, arg_stack);
        *defining = true;
        return func;
    }
    else if(equal_strings(name, "while")) {
        *defining = true;
        func = new_while();
    }
    else if(equal_strings(name, "print"))
        func = new_print();
    else
        func = new_function(name);

    func->function.function_data->args = arg_stack->stack;
    arg_stack->stack = NULL;

    if(equal_strings(name, "print")) {
        if(func->function.function_data->args->size != 1)
            error("Print must have exactly 1 argument");
        *defining = true;
        read_next(input_stream); //my_read semicolon
        return func;
    }
    if(*defining) {
        Stack *func_expr = new_stack();
        read_body(input_stream, func_expr, func_op);
        func->function.function_data->code = func_expr->stack;
        func_expr->stack = NULL; //So we can free stack and Objects without double free
    }
    return func;
}

void read_var_name(Object *input_stream, Buffer *b) {
    int c = stream_peek(input_stream);
    while(is_variable(c)) {
        read_next(input_stream);
        add_to_buffer(b, c);
        c = stream_peek(input_stream);
    }
    close_buffer(b);
}

void load_file(Object *input_stream) {
    skip_whitespace(input_stream);
    int c = read_next(input_stream);
    if(c != '"')
        error("Load file must have one string as argument");
    Buffer *b = new_buffer();
    c = read_next(input_stream);
    while(c != '"') {
        add_to_buffer(b, c);
        c = read_next(input_stream);
    }
    close_buffer(b);
    FILE *f = fopen(b->buffer, "r");
    if(!f)
        error("File not found");
    Object *fstream = new_load_file_stream(f);
    push(current_input_streams, fstream);

    REPL(fstream);
    char *filename = (char *)malloc((b->size + 1) * sizeof(char));
    strcpy(filename, b->buffer);
    printf("File %s was successfully loaded.\n", filename);
    print_symbol = true;
    free(filename);
    pop(current_input_streams); //so garbage collector can clean it
}

Object* read_array_indexes(Object *input_stream, Buffer *b) {
    bool reading_index = true;
    while(reading_index) {
        int c = read_next(input_stream);
        switch(c) {
            case '[': {
                break;
            }
            case ']': {
                add_to_buffer(b, ';');
                if(stream_peek(input_stream) != '[')
                    reading_index = false;
                break;
            }
            default: {
                add_to_buffer(b, c);
            }
        }
    }
    close_buffer(b);
    return new_string_stream(b->buffer);
}

void read_array_access(Object *input_stream, Stack *expr_stack, char *name) {
    Buffer *b = new_buffer();
    Object *input = read_array_indexes(input_stream, b);

    Stack *e_stack = new_stack();
    Stack *o_stack = new_stack();
    while(!end_of_stream(input)) {
        stream_read(input, e_stack, o_stack);
    }
    Object *ar = new_variable(name);
    ar->variable.index = e_stack->stack;
    e_stack->stack = NULL;
    push(expr_stack, ar);
}

//returns true if new function was defined
bool read_variable_or_func(Object *input_stream, Stack *expr_stack, int c) {
    Buffer *b = new_buffer();
    add_to_buffer(b, c);
    read_var_name(input_stream, b);

    skip_whitespace(input_stream);
    bool defining = false;
    bool call = false;
    c = stream_peek(input_stream);
    if(equal_strings(b->buffer, "def")) {
        defining = true;
        b = new_buffer();
        read_var_name(input_stream, b);
        if(equal_strings(b->buffer, "while") || equal_strings(b->buffer, "print") || equal_strings(b->buffer, "if") || equal_strings(b->buffer, "else") || equal_strings(b->buffer, "load_file"))
            error("Redefining system functions");
        delete_from_hash_table(global_env->functions, b->buffer);
    }
    else if(c == '.') {
        read_next(input_stream);
        c = read_next(input_stream);
        if(c == 't') {
            call = true;
        }
    }
    else if(equal_strings(b->buffer, "load_file")) {
        load_file(input_stream);
        return true;
    }
    skip_whitespace(input_stream);
    c = stream_peek(input_stream);
    if(c == '(') {
        read_next(input_stream);
        Object *func = read_func(input_stream, b->buffer, &defining);
        if(call) {
            if (getTag(func) == T_FUNCTION)
                func->function.measure_time = true;
            else
                error("Time can be measured only for functions");
        }
        push(expr_stack, func);
    }
    else if(c == '[') {
        read_next(input_stream);
        read_array_access(input_stream, expr_stack, b->buffer);
    }
    else {
        push(expr_stack, new_variable(b->buffer));
    }

    return defining;
}

void finish_read(Stack *expr_stack, Stack *operator_stack) {
    while(!empty_stack(operator_stack)) {
        if(expr_stack->stack->size < 2)
            error("Bad input (not anough objects in expr stack");
        Object *operator = top(operator_stack);
        pop(operator_stack);
        if(operator->operator.operator == '(')
            error("Input error: \'(\' out of place");
        Object *ex2 = top(expr_stack);
        pop(expr_stack);
        Object *ex1 = top(expr_stack);
        pop(expr_stack);
        push(expr_stack, new_node(operator, ex1, ex2));
    }
}

void read_array(Object *input_stream, Stack *expressions_stack) {
    if(stream_peek(input_stream) != ']')
        error("Expected ]");
    read_next(input_stream);
    Object *a = new_array_object();
    push(expressions_stack, a);
}

void close_brackets(Stack *expr_stack, Stack *operator_stack) {
    while(top(operator_stack)->operator.operator != '(') {
        if(expr_stack->stack->size < 2)
            error("Bad input (not anough objects in expr stack");
        Object *operator = top(operator_stack);
        pop(operator_stack);
        Object *ex2 = top(expr_stack);
        pop(expr_stack);
        Object *ex1 = top(expr_stack);
        pop(expr_stack);
        push(expr_stack, new_node(operator, ex1, ex2));
    }
    pop(operator_stack);
}

void stream_read(Object *input_stream, Stack *expr_stack, Stack *operator_stack) {
    skip_whitespace(input_stream);
    while(!end_of_stream(input_stream)) {
        if(object_heap_table->size >= HEAP_SIZE)
            error("Reached max heap size in my_read");

        int c = read_next(input_stream);

        switch (c) {
            case ';': {
                finish_read(expr_stack, operator_stack);
                return;
            }
            case '\"': {
                read_string(input_stream, expr_stack);
                break;
            }
            case '(': {
                push(operator_stack, new_operator('('));
                break;
            }
            case ')': {
                close_brackets(expr_stack, operator_stack);
                break;
            }
            case '[': {
                read_array(input_stream, expr_stack);
                break;
            }
            case '/': {
                if(stream_peek(input_stream) == '/') {
                    read_comment(input_stream);
                    break;
                }
            }
            default: {
                if (is_digit_or_negative(input_stream, c)) {
                    read_number(input_stream, expr_stack, c);
                } else if (is_operator(c)) {
                    read_operator(input_stream, expr_stack, operator_stack, c);
                }
                else if(is_variable(c)) {
                    if (read_variable_or_func(input_stream, expr_stack, c))
                        return;
                }
                else {
                    error("Unsupported char in my_read");
                }
            }
        }
        skip_whitespace(input_stream); //To recognize eof after whitespace
    }
}

Stack* my_read(Object *input_stream) {
    Stack *expr_stack = new_stack();
    Stack *operator_stack = new_stack();

    stream_read(input_stream, expr_stack, operator_stack);

    if(!read_finished(input_stream, expr_stack, operator_stack))
        error("Input did not end properly");
    return expr_stack;
}