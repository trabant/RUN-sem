//
// Created by david on 1/6/18.
//

#include <bits/time.h>
#include <time.h>
#include "../headers/functions.h"
#include "../headers/function_data.h"

Object* process_tree(Object *node, Environment *env);
Object* eval_array_access(Environment *env, Object *array, Array *indexes);

long timespecDiff(struct timespec *timeA_p, struct timespec *timeB_p)
{
    return ((timeA_p->tv_sec * 1000000000) + timeA_p->tv_nsec) -
           ((timeB_p->tv_sec * 1000000000) + timeB_p->tv_nsec);
}

Object *running_time(char *name, double time) {
    unsigned int buff_size = 256;
    char buffer[buff_size];
    int cx = snprintf(buffer, buff_size, "Function %s ran for %.6lf seconds", name, time); // nanoseconds
    if(cx >= 0 && cx < (signed)buff_size)
        return new_string(buffer);
    error("String with information about running time of function is longer then 255b, which is currently unsupported");
    return null;
}

bool object_true(Object *o, Environment *env) { // c-like
    Object *c = process_tree(o, env);
    switch (getTag(c)) {
        case T_NULL: {
            return false;
        }
        case T_INTEGER: {
            return c->integer.value != 0;
        }
        case T_BOOL: {
            return c->boolean.state;
        }
        default: {
            error("Unsupported argument in condition");
            return false;
        }
    }
}

Object *eval_variable(Environment *env, Object *variable) {
    Object *val = find_item(env->variables, variable);
    if(getTag(val) == T_NULL) {
        error("Variable was not found");
    }
    if(variable->variable.index != NULL)
        return eval_array_access(env, val, variable->variable.index);
    return val;
}

Object *value(Environment *env, Object *o) {
    if(getTag(o) == T_VARIABLE)
        return eval_variable(env, o);
    else if(getTag(o) == T_BOOL)
        return new_integer(o->boolean.state);
    return o;
}

int compare_integers(Object *left, Object *right, int operator) {
    switch(operator) {
        case '<': {
            return left->integer.value < right->integer.value;
        }
        case '>': {
            return left->integer.value > right->integer.value;
        }
        case '~': {
            return left->integer.value == right->integer.value;
        }
        default: {
            error("Unsupported operator for integer comparison");
            return 0;
        }
    }
}

int compare_strings(Object *left, Object *right, int operator) {
    int r = strcmp(left->string.value, right->string.value);
    switch(operator) {
        case '<': {
            return r < 0;
        }
        case '>': {
            return r > 0;
        }
        case '~': {
            return r == 0;
        }
        default: {
            error("Unsupported operator for string comparison");
            return 0;
        }
    }
}

Object *add_strings(Object *s1, Object *s2) {
    char *b = (char *)malloc((s1->string.length + s2->string.length + 1) * sizeof(char));
    sprintf(b, "%s%s", s1->string.value, s2->string.value);
    Object *s = new_string(b);
    free(b);
    return s;
}

Object *add_int_to_string(Object *string, Object *integer, bool before) {
    char *b = (char *)malloc((string->string.length + 21) * sizeof(char));
    if(before)
        sprintf(b, "%lu%s", integer->integer.value, string->string.value);
    else
        sprintf(b, "%s%lu", string->string.value, integer->integer.value);
    Object *s = new_string(b);
    free(b);
    return s;
}

Object* add_or_substract(Object *left, Object *right, int c) {
    if(getTag(left) == getTag(right)) {
        switch(getTag(left)) {
            case T_INTEGER: {
                return c == '+' ? new_integer(left->integer.value + right->integer.value) : new_integer(left->integer.value - right->integer.value);
            }
            case T_STRING: {
                if(c == '+')
                    return add_strings(left, right);
                error("Cannot substract strings");
                return null;
            }
            default: {
            }
        }
    }
    if(getTag(left) == T_INTEGER && getTag(right) == T_STRING) {
        return add_int_to_string(right, left, true);
    }
    if(getTag(right) == T_INTEGER && getTag(left) == T_STRING) {
        return add_int_to_string(left, right, false);
    }
    error("Bad data types in + or -");
    return null;
}

Object *multiply_string_by_int(Object *string, Object *integer) {
    char *b = (char *)malloc((string->string.length * integer->integer.value + 1) * sizeof(char));
    for(int i = 0; i < integer->integer.value; i++)
        sprintf(&b[i * string->string.length], "%s", string->string.value);
    Object *s = new_string(b);
    free(b);
    return s;
}

Object* multiply(Object *left, Object *right) {
    if(getTag(left) == getTag(right)) {
        switch(getTag(left)) {
            case T_INTEGER: {
                return new_integer(left->integer.value * right->integer.value);
            }
            default: {
            }
        }
    }
    if(getTag(left) == T_INTEGER && getTag(right) == T_STRING) {
        return multiply_string_by_int(right, left);
    }
    if(getTag(right) == T_INTEGER && getTag(left) == T_STRING) {
        return multiply_string_by_int(left, right);
    }
    error("Bad data types in multiplication");
    return null;
}

Object* divide(Object *left, Object *right) {
    if(getTag(left) != T_INTEGER || getTag(right) != T_INTEGER)
        error("Only numbers can be divided");
    if(right->integer.value == 0)
        error("Dividing by zero");
    return new_integer(left->integer.value / right->integer.value);
}

Object* eval_array_access(Environment *env, Object *array, Array *indexes) {
    Object *res = array;
    for(long i = 0; i < indexes->size; i++) {
        Object *index = process_tree(indexes->array[i], env);
        if (getTag(res) != T_ARRAY)
            error("Trying to access non-aray variable as array");
        if (getTag(index) != T_INTEGER)
            error("Array index must be a number");
        if (index->integer.value >= res->array.array->capacity) {
            grow_to_index(res->array.array, index->integer.value);
        }
        if (index->integer.value > res->array.max_index)
            res->array.max_index = index->integer.value;
        if (getTag(res->array.array->array[index->integer.value]) == T_NULL) {
            res->array.array->size++;
            res->array.array->array[index->integer.value] = new_object();
            res->array.array->array[index->integer.value]->tag.tag = T_NULL;
        }
        res = res->array.array->array[index->integer.value];
    }
    return res;
}

Object *assign_value(Environment *env, Object *left, Object *right) {
    if(getTag(left) != T_VARIABLE)
        error("You can assign only to variables\n");
    if(left->variable.index != NULL) {
        Object *array = find_item(env->variables, left);
        Object *o = eval_array_access(env, array, left->variable.index);
        shallow_copy(o, right);
        return o;
    }
    return insert_into_hash_table(env->variables, left->variable.name, right);
}

Object *math_operation(Environment *env, Object *left, Object *right, int operator) {
    Object* l = value(env, left);
    Object* r = value(env, right);
    switch(operator) {
        case '+':
        case '-': {
            return add_or_substract(l, r, operator);
        }
        case '*': {
            return multiply(l, r);
        }
        case '/': {
            return divide(l, r);
        }
        default: {
            error("Unsupported math operation");
            return null;
        }
    }
}

Object *eval_comparison(Environment *env, Object *left, Object *right, int operator) {
    int res = 0;
    Object* l = value(env, left);
    Object* r = value(env, right);
    if(getTag(l) == getTag(r)) {
        if(getTag(l) == T_INTEGER) {
            res = compare_integers(l, r, operator);
        }
        else if(getTag(l) == T_STRING) {
            res = compare_strings(l, r, operator);
        }
        else
            error("Unsupported comparison");
    }
    else
        error("Comparison between different data types");
    return new_bool(res == 1);
}

void define_function(Object *func) {
    Function_data *function = func->function.function_data;
    for(int i = 0; i < function->args->size; i++) {
        if(function->args->array[i]->tag.tag != T_VARIABLE)
            error("Bad argument given");
    }
    insert_into_hash_table(global_env->functions, func->function.name, func);
}

Object *perform_function(Environment *parent, Object *saved_function, Object *func) {
    Function_data *function_data = func_data(func);

    if (func_data(saved_function)->args->size != function_data->args->size)
        error("Bad number of arguments given");

    long f_length = func_data(saved_function)->code->size;
    if(f_length == 0)
        return null;
    for (int i = 0; i < function_data->args->size; i++) { //assign values to func args
        Object *arg = functions_arg(func, i);
        insert_into_hash_table(functions_env(saved_function)->variables,
                               functions_arg(saved_function, i)->variable.name, process_tree(arg, parent));
    }
    for (int i = 0; i < f_length - 1; i++) {
        process_tree(func_data(saved_function)->code->array[i], func_data(saved_function)->func_env);
    }
    return process_tree(func_data(saved_function)->code->array[f_length - 1], func_data(saved_function)->func_env);
}

Object *perform_while(Environment *env, Object *function) {
    Function_data *function_data = func_data(function);

    if(function_data->args->size != 1)
        error("While function must have exactly one argument");
    Object *ret = null;
    Object *cond = process_tree(functions_arg(function, 0), env);
    while(object_true(cond, env)) {

        long f_length = func_data(function)->code->size;

        for (int i = 0; i < f_length - 1; i++) {
            process_tree(func_data(function)->code->array[i], func_data(function)->func_env);
        }

        ret = process_tree(func_data(function)->code->array[f_length - 1], func_data(function)->func_env);
        cond = process_tree(functions_arg(function, 0), env);
    }
    return ret;
}

Object* eval_print(Environment *env, Object *function) {
    Object *res = process_tree(function->function.function_data->args->array[0], env);
    print(res, false);
    printf("\n");
    return res;
}

Object *eval_function(Environment *parent, Object *func) { // called from parent environment
    Object *saved_function;
    bool w = false;
    if(equal_strings(func->function.name, "print")) {
        return eval_print(parent, func);
    }
    if(equal_strings(func->function.name, "while")) { //while is in the same environment
        func->function.function_data->func_env = parent;
        w = true;
        saved_function = null;
    }
    else
        saved_function = find_item(global_env->functions, func);
    if(!w && getTag(saved_function) == T_NULL) {
        define_function(func);
        return func;
    }
    else {
        if(func->function.measure_time) {
            Object *res;
            struct timespec start, end;
            clock_gettime(CLOCK_MONOTONIC, &start);
            if(w)
                res = perform_while(parent, func);
            else
                res =  perform_function(parent, saved_function, func);
            clock_gettime(CLOCK_MONOTONIC, &end);
            Object *time = running_time(func->function.name, timespecDiff(&end, &start) / 1000000000.0);
            print(time, false);
            printf("\n");
            return res;
        }
        else {
            if(w)
                return perform_while(parent, func);
            else
                return perform_function(parent, saved_function, func);
        }
    }
}
Object *object_value(Environment *env, Object *o) {
    switch(getTag(o)) {
        case T_NODE: {
            return process_tree(o, env);
        }
        case T_FUNCTION: {
            return eval_function(env, o);
        }
        default: {
            return o;
        }
    }
}

Object* eval_condition(Environment *env, Object* cond) {
    Array *processed;
    if(object_true(cond->condition.condition, env))
        processed = cond->condition.if_code;
    else {
        if(cond->condition.else_code != NULL)
            processed = cond->condition.else_code;
        else
            return null;
    }
    long c_length = processed->size;
    if(!c_length)
        return null;
    for (int i = 0; i < c_length - 1; i++) {
        process_tree(processed->array[i], env);
    }
    return process_tree(processed->array[c_length - 1], env);
}

//recursive
Object* process_tree(Object *node, Environment *env) {
    if(object_heap_table->size >= HEAP_LIMIT)
        start_garbage_collection();
    Object *left;
    Object *right;
    if(getTag(node) != T_NODE) {
        if(getTag(node) == T_VARIABLE)
            return eval_variable(env, node);
        if(getTag(node) == T_FUNCTION)
            return eval_function(env, node);
        if(getTag(node) == T_CONDITION)
            return eval_condition(env, node);
        return node;
    }
    left = object_value(env, node->node.left);
    right = object_value(env, node->node.right);

    int operator = node->node.operator->operator.operator;
    switch(operator) {
        case '+':
        case '-':
        case '*':
        case '/': {
            return math_operation(env, left, right, operator);
        }
        case '=': {
            return assign_value(env, left, right);;
        }
        case '<':
        case '>':
        case '~': {
            return eval_comparison(env, left, right, operator);
        }
        default: {
            error("Evaluating unknow\n");
            return null; //To disable warnings from compiler
        }
    }
}

Object* eval(Stack *expr_stack) {
    Object *node = top(expr_stack);
    currently_evaluated = node;
    return process_tree(node, global_env);
}