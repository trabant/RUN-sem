//
// Created by david on 1/20/18.
//

#include "../headers/object.h"
#include "../headers/functions.h"

Function_data * func_data(Object *f) {
    if(getTag(f) != T_FUNCTION)
        error("Trying to get function data from nonfuction object");
    return f->function.function_data;
}