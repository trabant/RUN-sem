//
// Created by david on 1/24/18.
//

#include "../headers/helper_collections.h"
#include "../headers/object.h"

void insert_stack_to_helpers_heap(Stack *stack) {
    helpers_heap_table->field[helpers_heap_table->stack_pos++].stack = stack;
}

void insert_buffer_to_helpers_heap(Buffer *buffer) {
    helpers_heap_table->field[helpers_heap_table->buff_pos++].buffer = buffer;
}