//
// Created by david on 1/24/18.
//

#include "../headers/buffer.h"
#include "../headers/defaults.h"
#include "../headers/helper_collections.h"

Buffer* new_buffer() {
    Buffer *buffer = (Buffer *)malloc(sizeof(Buffer));
    buffer->buffer = (char *)malloc(INIT_BUFFER_SIZE * sizeof(char));
    buffer->size = INIT_BUFFER_SIZE;
    buffer->count = 0;
    insert_buffer_to_helpers_heap(buffer);
    return buffer;
}

void grow_buffer(Buffer *buffer) {
    buffer->size *= 2;
    buffer->buffer = (char *)realloc(buffer->buffer, buffer->size * sizeof(char));
}

void add_to_buffer(Buffer *buffer, int c) {
    buffer->buffer[buffer->count++] = (char)c;
    if (buffer->count == buffer->size)
        grow_buffer(buffer);
}

void close_buffer(Buffer *b) {
    add_to_buffer(b, '\0');
}
