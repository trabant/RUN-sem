//
// Created by david on 1/1/18.
//
#include <string.h>
#include "../headers/object.h"
#include "../headers/functions.h"
#include "../headers/function_data.h"
#include "../headers/helper_collections.h"

Object* new_file_stream(FILE *file) {
    Object *o = new_object();
    o->file_stream.tag.tag = T_FILE_STREAM;
    o->file_stream.input = file;
    o->file_stream.eof = false;
    o->file_stream.load_file = false;
    return o;
}

Object* new_load_file_stream(FILE *file) {
    Object *o = new_object();
    o->file_stream.tag.tag = T_FILE_STREAM;
    o->file_stream.input = file;
    o->file_stream.eof = false;
    o->file_stream.load_file = true;
    return o;
}
Object* new_string_stream(char *c) {
    Object *o = new_object();
    o->string_stream.tag.tag = T_STRING_STREAM;
    o->string_stream.input = (char *)malloc((strlen(c) + 1) * sizeof(char));
    o->string_stream.origin = o->string_stream.input;
    strcpy(o->string_stream.input, c);
    return o;
}

Object* new_string(char *str) {
    Object *string = new_object();
    string->string.tag.tag = T_STRING;
    string->string.value = (char *)malloc((strlen(str) + 1) * sizeof(char));
    strcpy(string->string.value, str);
    string->string.length = strlen(str);
    return string;
}

Object* new_integer(long val) {
    Object *integer = new_object();
    integer->integer.tag.tag = T_INTEGER;
    integer->integer.value = val;
    return integer;
}

Object *new_variable(const char *str) {
    Object *variable = new_object();
    variable->variable.tag.tag = T_VARIABLE;
    variable->variable.name = (char *)malloc((strlen(str) + 1) * sizeof(char));
    strcpy(variable->variable.name, str);
    variable->variable.index = NULL;
    return variable;
}

Object *new_condition() {
    Object *condition = new_object();
    condition->condition.tag.tag = T_CONDITION;
    condition->condition.condition = NULL;
    condition->condition.if_code = NULL;
    condition->condition.else_code = NULL;
    return condition;
}

Object *new_function(char *str) {
    Object *function = new_object();
    function->function.tag.tag = T_FUNCTION;
    function->function.measure_time = false;
    function->function.owns_env = true;
    function->function.name = (char *)malloc((strlen(str) + 1) * sizeof(char));
    strcpy(function->function.name, str);
    function->function.function_data = new_function_data();
    return function;
}

Object *new_array_object() {
    Object *array = new_object();
    array->array.tag.tag = T_ARRAY;
    array->array.array = NULL;
    array->array.max_index = -1;
    return array;
}

Object *new_while() {
    Object *function = new_object();
    function->function.tag.tag = T_FUNCTION;
    function->function.measure_time = false;
    function->function.owns_env = false;
    function->function.name = (char *)malloc(6 * sizeof(char)); // while + 1
    strcpy(function->function.name, "while");
    function->function.function_data = new_while_data();
    return function;
}

Object *new_print() {
    Object *function = new_object();
    function->function.tag.tag = T_FUNCTION;
    function->function.measure_time = false;
    function->function.owns_env = false;
    function->function.name = (char *)malloc(6 * sizeof(char)); // while + 1
    strcpy(function->function.name, "print");
    function->function.function_data = new_while_data();
    return function;
}

Object* new_operator(char c) {
    Object *o = new_object();
    o->operator.tag.tag = T_OPERATOR;
    o->operator.operator = c;
    switch(c) {
        case '+':
        case '-': {
            o->operator.precedence = 3;
            break;
        }
        case '*':
        case '/': {
            o->operator.precedence = 4;
            break;
        }
        case '(':
        case ')': {
            o->operator.precedence = 1;
            break;
        }
        case '=': {
            o->operator.precedence = -1;
            break;
        }
        case '~': { // ==
            o->operator.precedence = 0;
            break;
        }
        case '<':
        case '>': {
            o->operator.precedence = 2;
            break;
        }
        default: {
            error("Unknown operator");
        }
    }
    return o;
}

Object* new_node(Object *operator, Object *left, Object *right) {
    Object *node = new_object();
    node->node.operator = operator;
    node->node.left = left;
    node->node.right = right;
    node->node.tag.tag = T_NODE;
    return node;
}

Object* new_NULL() {
    if(null)
        return null;
    Object *n = (Object *)malloc(sizeof(Object));
    n->tag.tag = T_NULL;
    n->tag.marked = false;
    return n;
}

Object* new_bool(bool b) {
    return b ? my_true : my_false;
}

Object* prepare_bool(bool b) {
    Object *n = (Object *)malloc(sizeof(Object));
    n->boolean.tag.tag = T_BOOL;
    n->boolean.tag.marked = false;
    n->boolean.state = b;
    return n;
}

Object* new_object() {
    Object *o = (Object *)malloc(sizeof(Object));
    o->tag.marked = false;
    o->tag.tag = T_NULL;
    store_on_heap(o);
    return o;
}

Function_data* new_function_data() {
    Function_data *f = (Function_data *)malloc(sizeof(Function_data));
    f->args = NULL;
    f->code = NULL;
    f->func_env = new_function_environment();
    return f;
}

Function_data* new_while_data() {
    Function_data *f = (Function_data *)malloc(sizeof(Function_data));
    f->args = NULL;
    f->code = NULL;
    f->func_env = NULL;
    return f;
}

Environment* new_environment() {
    Environment *env = (Environment *)malloc(sizeof(Environment));
    env->functions = init_hash_table();
    env->variables = init_hash_table();
    return env;
}

Environment* new_function_environment() {
    Environment *env = (Environment *)malloc(sizeof(Environment));
    env->functions = NULL;
    env->variables = init_hash_table();
    return env;
}

Environment* functions_env(Object *func) {
    return func->function.function_data->func_env;
}

Object* functions_arg(Object *func, int index) {
    return func->function.function_data->args->array[index];
}

void shallow_copy(Object *o1, Object *o2) {
    // free o1
    switch(getTag(o1)) {
        case T_VARIABLE: {
            free(o1->variable.name);
            break;
        }
        case T_STRING: {
            free(o1->string.value);
            break;
        }
        case T_ARRAY: {
            free_array(o1->array.array);
            break;
        }
        default: {
            break;
        }
    }
    // copy
    o1->tag.tag = o2->tag.tag;
    switch(getTag(o2)) {
        case T_INTEGER: {
            o1->integer.value = o2->integer.value;
            break;
        }
        case T_STRING: {
            o1->string.value = (char *)malloc((o2->string.length + 1) * sizeof(char));
            strcpy(o1->string.value, o2->string.value);
            o1->string.length = o2->string.length;
            break;
        }
        case T_NULL: {
            o1->tag.tag = T_NULL;
            break;
        }
        case T_ARRAY: {
            if(o2->array.array == NULL)
                o1->array.array = new_array(); //o2 array is null on purpose (gc problems)
            else
                o1->array.array = o2->array.array;
            o1->array.max_index = o2->array.max_index;
            break;
        }
        default: {
            error("Unsupported shallow assingment");
        }
    }
}


bool equal_strings(const char *a, const char *b) {
    return strcmp(a, b) == 0;
}

bool is_load_file_stream(Object *input_stream) {
    if(getTag(input_stream) == T_STRING_STREAM)
        return false;
    return input_stream->file_stream.load_file;
}

void prepare_to_run() {
    print_symbol = true;
    global_env = new_environment();
    my_true = prepare_bool(true);
    my_false = prepare_bool(false);
    currently_evaluated = null = NULL; // Just to be sure
    current_input_streams = new_global_stack();
    null = new_NULL();
    object_heap_table = new_heap();
    helpers_heap_table = new_helpers_heap();
    insert_into_hash_table(global_env->functions, "while", new_function("while"));
    insert_into_hash_table(global_env->functions, "print", new_function("print"));
}

void REPL(Object *input_stream) {
    Object *to_print;
    for (;;) {
        if(!is_load_file_stream(input_stream) && print_symbol) {
            printf("> ");
            print_symbol = false;
        }
        Stack *expr_stack = my_read(input_stream);

        if(expr_stack->stack->size) {

            to_print = eval(expr_stack);

            if(!is_load_file_stream(input_stream))
                print(to_print, true);
            currently_evaluated = NULL;
        }
        if (stream_finished(input_stream)) {
            return;
        }
    }
}

void error(const char *c) {
    fprintf(stderr, "%s\n", c);
    abort();
//    free_all();
}