//
// Created by david on 1/3/18.
//

#include "../headers/array.h"
#include "../headers/defaults.h"

Array* new_array() {
    Array *array = (Array*)malloc(sizeof(Array));
    array->array = (Object**)malloc(sizeof(Object*) * INIT_ARRAY_SIZE);
    for(int i = 0; i < INIT_ARRAY_SIZE; i++)
        array->array[i] = null;
    array->capacity = INIT_ARRAY_SIZE;
    array->size = 0;
    return array;
}

void set_item(Array *array, long index, Object *item) {
    array->array[index] = item;
}

void grow_array(Array *array) {
    long old = array->capacity;
    array->capacity *= 2;
    array->array = (Object **)realloc(array->array, array->capacity * sizeof(Object*));
    for(long i = old; i < array->capacity; i++)
        array->array[i] = null;
}

void grow_to_index(Array *array, long index) {
    long old = array->capacity;
    if(index < array->capacity * 2)
        array->capacity *= 2;
    else
        array->capacity = index + 1;
    array->array = (Object **)realloc(array->array, array->capacity * sizeof(Object*));
    for(long i = old; i < array->capacity; i++)
        array->array[i] = null;
}

bool empty_array(Array *array) {
    return array->size == 0;
}

bool full_array(Array *array) {
    return array->size == array->capacity;
}

void free_array(Array *array) {
//    for(int i = 0; i < array->capacity; i++) {
//        if(array->array[i] != NULL) {
//            free_object(array->array[i]);
//            array->array[i] = NULL;
//        }
//    }
    free(array->array);
    free(array);
    array = NULL;
}