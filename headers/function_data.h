//
// Created by david on 1/14/18.
//

#ifndef SEM_FUNCTION_H
#define SEM_FUNCTION_H

#include "environment.h"
#include "array.h"

struct Function_data {
    Environment *func_env; // only for while and conditions
    Array *code;
    Array *args;
};

Function_data * func_data(Object *f);

#endif //SEM_FUNCTION_H
