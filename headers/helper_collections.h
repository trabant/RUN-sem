//
// Created by david on 1/24/18.
//

#ifndef SEM_HELPER_COLLECTIONS_H
#define SEM_HELPER_COLLECTIONS_H


typedef struct Stack Stack;
typedef struct Buffer Buffer;

typedef struct helper_field {
    Stack *stack;
    Buffer *buffer;
} helper_field;

typedef struct Helpers_collection {
    int stack_pos;
    int buff_pos;
    helper_field *field;
} Helpers_collection;

Helpers_collection* new_helpers_heap();
void insert_stack_to_helpers_heap(Stack *stack);
void insert_buffer_to_helpers_heap(Buffer *buffer);

#endif //SEM_HELPER_COLLECTIONS_H
