//
// Created by david on 1/3/18.
//

#ifndef SEM_STACK_H
#define SEM_STACK_H

#include <stdbool.h>
#include "object.h"
#include "array.h"

typedef struct Stack {
    Array *stack;
} Stack;

Stack* new_stack();
Stack* new_global_stack();
void push(Stack *, Object *);
void pop(Stack *);
Object* top(Stack *);
bool empty_stack(Stack *);

#endif //SEM_STACK_H
