//
// Created by david on 1/14/18.
//

#ifndef SEM_ENVIRONMENT_H
#define SEM_ENVIRONMENT_H

#include "hash_table.h"

typedef struct Environment Environment;

struct Environment {
    HashTable *variables;
    HashTable *functions;
};

#endif //SEM_ENVIRONMENT_H
