//
// Created by david on 1/7/18.
//

#ifndef SEM_HASH_TABLE_H
#define SEM_HASH_TABLE_H

#include <string.h>
#include "object.h"
#include "defaults.h"

static const unsigned int primes[] = {
        67, 131, 257, 521, 1031, 2053, 4099, 8209, 16411, 32771,
        65537, 131101, 262147, 524309
};

typedef struct HashField {
    Object *value;
    char *key;
    bool toomstomb;
} HashField;

typedef struct HashTable {
    long capacity;
    long size;
    HashField *table;

} HashTable;

void fill_with_null(HashTable *table);
HashTable *init_hash_table();
HashTable *new_hash_table(long capacity);
long next_prime(long num);
long hash(char *str, long mod);
void grow_hash_table(HashTable *table);
Object* insert_into_hash_table(HashTable *table, char *key, Object *value);
Object* find_item(HashTable *table, Object *item);
long find_position(HashTable *table, char *key);
void delete_from_hash_table(HashTable *, char *);
void free_hash_table(HashTable *);

#endif //SEM_HASH_TABLE_H
