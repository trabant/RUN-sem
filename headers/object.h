//
// Created by david on 1/1/18.
//

#ifndef SEM_TYPES_H
#define SEM_TYPES_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct Environment Environment;
typedef struct Array Array;
typedef struct Stack Stack;
typedef struct Function_data Function_data;
typedef struct Helpers_collection Helpers_collection;

typedef enum objectTag {
    T_INTEGER,
    T_STRING,
    T_VARIABLE,
    T_FUNCTION,
    T_NULL,
    T_NODE,
    T_FILE_STREAM,
    T_STRING_STREAM,
    T_OPERATOR,
    T_CONDITION,
    T_ARRAY,
    T_BOOL
} objectTag;

typedef union myObject Object;

// Globals
Object *null;
Environment *global_env;
Object *my_true;
Object *my_false;
Array *object_heap_table;
Helpers_collection *helpers_heap_table;
Object *currently_evaluated; //Dont free running while etc
Stack *current_input_streams;
bool print_symbol;
//Array *buffer_heap_table; //Rozmyslet

struct myTag {
    enum objectTag tag;
    bool marked;
};

struct myInt {
    struct myTag tag;
    long value;
};

struct myString {
    struct myTag tag;
    char *value;
    long length;
};

struct myVariable {
    struct myTag tag;
    Array *index;
    char *name;
};

struct myFunction {
    struct myTag tag;
    char *name;
    bool measure_time;
    bool owns_env;
    Function_data *function_data;
};

struct myOperator {
    struct myTag tag;
    char operator;
    int precedence;
};

struct Node {
    struct myTag tag;
    Object *operator;

    Object *left;
    Object *right;
};

struct myCondition {
    struct myTag tag;
    Object *condition;
    Array *if_code;
    Array *else_code;
};

struct myFileStream {
    struct myTag tag;
    FILE *input;
    bool eof;
    bool load_file;
};

struct myStringStream {
    struct myTag tag;
    char *input;
    char *origin;
};

struct myArray {
    struct myTag tag;
    long max_index;
    Array *array;
};

struct myBool {
    struct myTag tag;
    bool state;
};

union myObject {
    struct myInt integer;
    struct myString string;
    struct myVariable variable;
    struct myFunction function;
    struct myFileStream file_stream;
    struct myStringStream string_stream;
    struct myOperator operator;
    struct myCondition condition;
    struct Node node;
    struct myArray array;
    struct myBool boolean;
    struct myTag tag;
};

#endif //SEM_TYPES_H