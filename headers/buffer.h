//
// Created by david on 1/24/18.
//

#ifndef SEM_BUFFER_H
#define SEM_BUFFER_H

#include <stdlib.h>

typedef struct Buffer {
    char * buffer;
    int size;
    int count;
} Buffer;

Buffer* new_buffer();

void grow_buffer(Buffer *buffer);

void add_to_buffer(Buffer *buffer, int c);

void close_buffer(Buffer *b);

#endif //SEM_BUFFER_H
