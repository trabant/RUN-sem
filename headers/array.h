//
// Created by david on 1/3/18.
//

#ifndef SEM_ARRAY_H
#define SEM_ARRAY_H

#include "object.h"

struct Array {
    long capacity;
    long size;
    Object **array;
};

Array* new_array();
Array* new_heap();
void set_item(Array *, long, Object *);
void grow_array(Array *);
void grow_to_index(Array *array, long index);
bool empty_array(Array *array);
bool full_array(Array *);
void free_array(Array *);

#endif //SEM_ARRAY_H
