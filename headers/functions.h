//
// Created by david on 1/1/18.
//

#ifndef SEM_FUNCTIONS_H
#define SEM_FUNCTIONS_H

#include "object.h"
#include "stack.h"
#include "hash_table.h"
#include "environment.h"

Object* new_file_stream(FILE*);
Object* new_load_file_stream(FILE*);
Object* new_string_stream(char *);
Object* new_string(char *);
Object* new_integer(long);
Object* new_operator(char);
Object* new_variable(const char *);
Object* new_condition();
Object* new_function(char *);
Object* new_array_object();
Object* new_while();
Object* new_print();
Object* new_node(Object *operator, Object *left, Object *right);
Object* new_NULL();
Object* new_object();
Object* new_bool(bool);
Object* prepare_bool(bool b);
Function_data* new_function_data();
Function_data* new_while_data();
Environment* new_environment();
Environment* new_function_environment();
Environment* functions_env(Object *func);
Object *functions_arg(Object *, int index);
void shallow_copy(Object *, Object *);
void REPL(Object *);
Stack* my_read(Object *);
Object* eval(Stack *);
void print(Object *, bool);
void store_on_heap(Object *);
void start_garbage_collection();
bool free_object(Object *);
void error(const char *c);
bool equal_strings(const char *, const char *);
void run_tests();
void free_all();
void prepare_to_run();
bool end_of_stream(Object *input_stream);
bool is_load_file_stream(Object *);
bool stream_finished(Object *input_stream);
long timespecDiff(struct timespec *timeA_p, struct timespec *timeB_p);
static inline objectTag getTag(Object *o) {
    return o->tag.tag;
}

#endif //SEM_FUNCTIONS_H
