#include <bits/signum.h>
#include <signal.h>
#include "headers/object.h"
#include "headers/functions.h"


Object *input_stream;

void catch_abort(int signal_number) {
    printf("\n> ");
    REPL(input_stream);

    free_all();
}

int main() {
    signal(SIGABRT, &catch_abort);
    prepare_to_run();
    run_tests();
    input_stream = new_file_stream(stdin);
    push(current_input_streams, input_stream);

    REPL(input_stream);

    free_all();
    return 0;
}